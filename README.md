### DEMO
El demo consiste en construir un servicio que tome un serial y pueda realizar validaciones
de forma efectiva sobre el mismo.

El serial tiene la siguiente estructura:

[generator_type][generator_id]-[inverted_phone_number]-[charge_id]

generator_type = Alfabetico, longitud 2
generator_id = Numerico, longitud 7
inverted_phone_number = Numerico, longitud 9
charge_id = Alphanumeric, longitud 13

Por ejemplo, EM1234567-370440869-123F5678M1234

generator_type = EM
generator_id = 1234567
inverted_phone_number = 370440869
charge_id = 123F5678M1234  

 

