package com.everis.firstdemotdd.client;

public interface ChargeIdVerifier {
    Boolean verifyChargeId(String id);
}
