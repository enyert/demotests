package com.everis.firstdemotdd.client.impl;

import com.everis.firstdemotdd.client.ChargeIdVerifier;

public class ChargeIdVerifierImpl implements ChargeIdVerifier {

    private static final String REGEX = "[A-Za-z0-9]+";

    @Override
    public Boolean verifyChargeId(String id) {
        return !id.isEmpty() && id.length() == 13 && id.matches("[A-Za-z0-9]+");
    }
}
