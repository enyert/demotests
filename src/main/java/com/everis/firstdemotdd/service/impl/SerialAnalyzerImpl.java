package com.everis.firstdemotdd.service.impl;

import com.everis.firstdemotdd.model.SerialModel;
import com.everis.firstdemotdd.service.SerialAnalyzer;
import org.springframework.stereotype.Service;

@Service
public class SerialAnalyzerImpl implements SerialAnalyzer {

    @Override
    public Boolean validateSerial(String serial) {
        SerialModel serialModel = new SerialModel(serial);
        return serialModel.validate();
    }
}
