package com.everis.firstdemotdd.service;

public interface SerialAnalyzer {
    Boolean validateSerial(String serial);
}