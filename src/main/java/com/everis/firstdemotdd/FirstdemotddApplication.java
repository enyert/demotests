package com.everis.firstdemotdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstdemotddApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstdemotddApplication.class, args);
	}
}
