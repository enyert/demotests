package com.everis.firstdemotdd.model;

import com.everis.firstdemotdd.format.SerialExtractor;
import com.everis.firstdemotdd.validation.ComponentValidator;

public class SerialModel {

    private String generatorType;
    private String generatorId;
    private String phoneNumber;
    private String rechargeId;

    public SerialModel() {
    }

    public SerialModel(String serial) {
        this.generatorType = SerialExtractor.extractGeneratorType(serial);
        this.generatorId = SerialExtractor.extractGeneratorId(serial);
        this.phoneNumber = SerialExtractor.extractPhoneNumber(serial);
        this.rechargeId = SerialExtractor.extractRechargeId(serial);
    }

    public String getGeneratorType() {
        return generatorType;
    }

    public void setGeneratorType(String generatorType) {
        this.generatorType = generatorType;
    }

    public String getGeneratorId() {
        return generatorId;
    }

    public void setGeneratorId(String generatorId) {
        this.generatorId = generatorId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRechargeId() {
        return rechargeId;
    }

    public void setRechargeId(String rechargeId) {
        this.rechargeId = rechargeId;
    }

    public Boolean validate() {
        return ComponentValidator.validateGeneratorId(this.generatorId) &&
                ComponentValidator.validateGeneratorType(this.generatorType) &&
                ComponentValidator.validatePhoneNumber(this.phoneNumber) &&
                ComponentValidator.validateRechargeId(this.rechargeId);
    }
}
