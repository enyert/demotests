package com.everis.firstdemotdd.controller;

import com.everis.firstdemotdd.service.SerialAnalyzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/entel-serial-validator")
@RestController
public class ServiceAnalyzerController {

    @Autowired
    SerialAnalyzer serialAnalyzer;

    @GetMapping
    @ResponseBody
    public Boolean analyzeSerial(@RequestParam String serial) {
        Boolean result = serialAnalyzer.validateSerial(serial);
        return result;
    }
}
