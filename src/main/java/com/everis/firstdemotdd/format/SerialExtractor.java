package com.everis.firstdemotdd.format;

public class SerialExtractor {
    public static String extractPhoneNumber(String serial) {
        //return serial.split("-")[1];
        return new StringBuilder(serial.split("-")[1]).reverse().toString();
        //return null;
    }

    public static String extractGeneratorType(String serial) {
        return serial.split("-")[0].substring(0, 2);
        //return null;
    }

    public static String extractGeneratorId(String serial) {
        return serial.split("-")[0].substring(2, 9);
        //return null;
    }

    public static String extractRechargeId(String serial) {
        return serial.split("-")[2];
        //return null;
    }


}
