package com.everis.firstdemotdd.validation;

import org.apache.commons.lang3.StringUtils;

public class ComponentValidator {
    public static Boolean validatePhoneNumber(String phoneNumber) {
        //return null;
        //return phoneNumber != null;
        //return phoneNumber != null && !phoneNumber.isEmpty();
        //return phoneNumber != null && !phoneNumber.isEmpty() && phoneNumber.length() == 9;
        return StringUtils.isNumeric(phoneNumber) && phoneNumber != null && !phoneNumber.isEmpty() && phoneNumber.length() == 9;
    }

    public static Boolean validateGeneratorType(String generatorType) {
        //return null;
        //return generatorType != null;
        //return generatorType != null && !generatorType.isEmpty();
        //return generatorType != null && !generatorType.isEmpty() && generatorType.length() == 2;
        return generatorType != null && !generatorType.isEmpty() && generatorType.length() == 2 && StringUtils.isAlpha(generatorType);
    }


    public static Boolean validateGeneratorId(String generatorId) {
        //return null;
        //return generatorId != null;
        //return generatorId != null && !generatorId.isEmpty();
        //return generatorId != null && !generatorId.isEmpty(); && generatorId.length() == 7;
        return generatorId != null && !generatorId.isEmpty() && generatorId.length() == 7 && StringUtils.isAlphanumeric(generatorId);
    }

    public static Boolean validateRechargeId(String rechargeId) {
        //return null;
        //return rechargeId != null;
        //return rechargeId != null && !rechargeId.isEmpty();
        return rechargeId != null && !rechargeId.isEmpty() && rechargeId.length() == 13 && StringUtils.isAlphanumeric(rechargeId);
    }
}

