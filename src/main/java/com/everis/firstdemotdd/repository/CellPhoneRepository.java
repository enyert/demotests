package com.everis.firstdemotdd.repository;

public interface CellPhoneRepository {
    Boolean searchCellPhoneNumber(String cellPhone);
}
