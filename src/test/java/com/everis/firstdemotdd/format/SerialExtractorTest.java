package com.everis.firstdemotdd.format;

import com.everis.firstdemotdd.validation.ComponentValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SerialExtractorTest {

    String input;

    @Before
    public void setUp() throws Exception {
        input = "EM1234567-370440869-123F5678M1234";
    }

    @Test
    public void extractPhoneNumberTest() {
        String expectedResult = "968044073";
        String result = SerialExtractor.extractPhoneNumber(input);
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void extractGeneratorType() {
        String expectedResult = "EM";
        String result = SerialExtractor.extractGeneratorType(input);
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void extractGeneratorIdTest() {
        String expectedResult = "1234567";
        String result = SerialExtractor.extractGeneratorId(input);
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void extractRechargeIdTest() {
        String expectedResult = "123F5678M1234";
        String result = SerialExtractor.extractRechargeId(input);
        Assert.assertEquals(result, expectedResult);
    }
}
