package com.everis.firstdemotdd.client;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChargeIdVerifierTest {
    ChargeIdVerifier chargeIdVerifier;

    @Before
    public void setUp() throws Exception {

        chargeIdVerifier = mock(ChargeIdVerifier.class);

        when(chargeIdVerifier.verifyChargeId("12345678910111213")).thenReturn(true);

        when(chargeIdVerifier.verifyChargeId("")).thenReturn(false);
        when(chargeIdVerifier.verifyChargeId("23456654")).thenReturn(false);
        when(chargeIdVerifier.verifyChargeId("123456789101112167")).thenReturn(false);
    }

    @Test
    public void validateRechargeIdTest() {
        Boolean firstResult = chargeIdVerifier.verifyChargeId("12345678910111213");
        Boolean secondResult = chargeIdVerifier.verifyChargeId("");
        Boolean thirdResult = chargeIdVerifier.verifyChargeId("23456654");
        Boolean fourthResult = chargeIdVerifier.verifyChargeId("123456789101112167");
        Assert.assertTrue(firstResult);
        Assert.assertFalse(secondResult);
        Assert.assertFalse(thirdResult);
        Assert.assertFalse(fourthResult);
    }
}
