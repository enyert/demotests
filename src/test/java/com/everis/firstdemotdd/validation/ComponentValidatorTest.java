package com.everis.firstdemotdd.validation;

import com.everis.firstdemotdd.client.ChargeIdVerifier;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ComponentValidatorTest {

    String input;

    @Test
    public void validatePhoneNumber() {
        input = "968044073";
        Boolean result = ComponentValidator.validatePhoneNumber(input);
        Assert.assertTrue(result);
    }

    @Test
    public void validateGeneratorType() {
        input = "EM";
        Boolean result = ComponentValidator.validateGeneratorType(input);
        Assert.assertTrue(result);
    }

    @Test
    public void validateGeneratorId() {
        input = "1234567";
        Boolean result = ComponentValidator.validateGeneratorId(input);
        Assert.assertTrue(result);
    }

    @Test
    public void validateRechargeId() {
    }
}

//EM1234567-370440869-123F5678M1234
